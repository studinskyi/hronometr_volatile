package com.inno.studinsky.hronometr;

import java.util.Date;

public class Hronometr extends Thread {
    //private Date startTime; // момент старта потока хронометра
    private static volatile int timeWork; // количество секунд с момента начала работы программы
    private Boolean itIsClock = false;
    private int periodShowTime = 1;

    public Hronometr() {
        setPriority(MAX_PRIORITY);
        //startTime = new Date();
        start();
    }

    public void setItIsClock(Boolean itIsClock) {
        this.itIsClock = itIsClock;
    }

    public void setPeriodShowTime(int periodShowTime) {
        this.periodShowTime = periodShowTime;
    }

    public void run() {
        try {
            while (!Main.isStopped) {
                if (!itIsClock) {
                    timeWork = timeWork + 1;
                    Thread.sleep(1000);
                    //timeWork = addTime(timeWork);
                    printHrono();
                }
                if (itIsClock) {
                    if (timeWork % periodShowTime == 0 && timeWork > 0) {
                        System.out.println("отсчет периодичности  " + periodShowTime + " текущая секунда" + timeWork);
                        Thread.sleep(periodShowTime * 1000);
                    }
                }


                if (timeWork >= 22)
                    Main.isStopped = true;
            }
        } catch (InterruptedException e) {
            System.out.println("Поток экземпляра объекта Hronometr был прерван по InterruptedException!");
        }
    }

//    public synchronized static Integer addTime(int currentTime) {
//        currentTime = currentTime + 1;
//        return currentTime;
//    }

    private void printHrono() throws InterruptedException {
        System.out.println("хронометр " + timeWork);
        //        Thread.sleep(1000);
        //        timeWork = timeWork + 1;
    }
}
